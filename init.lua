discrete_player = {}
discrete_player.bank = {}

discrete_player.default = {
	-- The Walking speed
	walk_speed = 1,
	-- Can the player use his keyboard to move?
	active = true,
	
	-- This stuff isn't designed to be overriden.
	
	player = nil, -- Player's name
	target_pos = nil, -- The coordinate towards which the player is walking to
	walking_direction = nil, -- The walking direction or nil when standing
	directions = { -- All possible directions
		xp = {x=1, y=0, z=0},
		xn = {x=-1, y=0, z=0},
		yp = {x=0, y=1, z=0},
		yn = {x=0, y=-1, z=0},
		zp = {x=0, y=0, z=1},
		zn = {x=0, y=0, z=-1},
	},
	walk_stop_functions = { -- Called to stop movement when the target position is reached.
		xp = function(self, pos)
			if pos.x >= self.target_pos then
				self.object:set_velocity({x=0, y=0, z=0})
				self.walking_direction = nil
				self.object:set_pos({x=self.target_pos, y=pos.y, z=pos.z})
				self:on_walk_finish()
			end
		end,
		xn = function(self, pos)
			if pos.x <= self.target_pos then
				self.object:set_velocity({x=0, y=0, z=0})
				self.walking_direction = nil
				self.object:set_pos({x=self.target_pos, y=pos.y, z=pos.z})
				self:on_walk_finish()
			end
		end,
		yp = function(self, pos)
			if pos.y >= self.target_pos then
				self.object:set_velocity({x=0, y=0, z=0})
				self.walking_direction = nil
				self.object:set_pos({x=pos.x, y=self.target_pos, z=pos.z})
				self:on_walk_finish()
			end
		end,
		yn = function(self, pos)
			if pos.y <= self.target_pos then
				self.object:set_velocity({x=0, y=0, z=0})
				self.walking_direction = nil
				self.object:set_pos({x=pos.x, y=self.target_pos, z=pos.z})
				self:on_walk_finish()
			end
		end,
		zp = function(self, pos)
			if pos.z >= self.target_pos then
				self.object:set_velocity({x=0, y=0, z=0})
				self.walking_direction = nil
				self.object:set_pos({x=pos.x, y=pos.y, z=self.target_pos})
				self:on_walk_finish()
			end
		end,
		zn = function(self, pos)
			if pos.z <= self.target_pos then
				self.object:set_velocity({x=0, y=0, z=0})
				self.walking_direction = nil
				self.object:set_pos({x=pos.x, y=pos.y, z=self.target_pos})
				self:on_walk_finish()
			end
		end,
	},
	end_walk_functions = { -- Called to finish walking immediatelly
		xp = function(self)
			local pos = self.object:get_pos()
			self.object:set_velocity({x=0, y=0, z=0})
			self.walking_direction = nil
			self.object:set_pos({x=self.target_pos, y=pos.y, z=pos.z})
			self:on_walk_finish()
		end,
		xn = function(self)
			local pos = self.object:get_pos()
			self.object:set_velocity({x=0, y=0, z=0})
			self.walking_direction = nil
			self.object:set_pos({x=self.target_pos, y=pos.y, z=pos.z})
			self:on_walk_finish()
		end,
		yp = function(self)
			local pos = self.object:get_pos()
			self.object:set_velocity({x=0, y=0, z=0})
			self.walking_direction = nil
			self.object:set_pos({x=pos.x, y=self.target_pos, z=pos.z})
			self:on_walk_finish()
		end,
		yn = function(self)
			local pos = self.object:get_pos()
			self.object:set_velocity({x=0, y=0, z=0})
			self.walking_direction = nil
			self.object:set_pos({x=pos.x, y=self.target_pos, z=pos.z})
			self:on_walk_finish()
		end,
		zp = function(self)
			local pos = self.object:get_pos()
			self.object:set_velocity({x=0, y=0, z=0})
			self.walking_direction = nil
			self.object:set_pos({x=pos.x, y=pos.y, z=self.target_pos})
			self:on_walk_finish()
		end,
		zn = function(self)
			local pos = self.object:get_pos()
			self.object:set_velocity({x=0, y=0, z=0})
			self.walking_direction = nil
			self.object:set_pos({x=pos.x, y=pos.y, z=self.target_pos})
			self:on_walk_finish()
		end,
	},
	start_walking_functions = { -- Called to start walking
		xp = function(self, pos)
			self.object:set_velocity({x=self.walk_speed, y=0, z=0})
			self.walking_direction = "xp"
			self.target_pos = pos.x + 1
		end,
		xn = function(self, pos)
			self.object:set_velocity({x=-self.walk_speed, y=0, z=0})
			self.walking_direction = "xn"
			self.target_pos = pos.x - 1
		end,
		yp = function(self, pos)
			self.object:set_velocity({x=0, y=self.walk_speed, z=0})
			self.walking_direction = "yp"
			self.target_pos = pos.y + 1
		end,
		yn = function(self, pos)
			self.object:set_velocity({x=0, y=-self.walk_speed, z=0})
			self.walking_direction = "yn"
			self.target_pos = pos.y - 1
		end,
		zp = function(self, pos)
			self.object:set_velocity({x=0, y=0, z=self.walk_speed})
			self.walking_direction = "zp"
			self.target_pos = pos.z + 1
		end,
		zn = function(self, pos)
			self.object:set_velocity({x=0, y=0, z=-self.walk_speed})
			self.walking_direction = "zn"
			self.target_pos = pos.z - 1
		end,
	},
}

--[[*************************************************
    * These functions are designed to be overriden. *
    *************************************************]]--

-- Called when the player is trying to walk. Returns true when the player is allowed to walk that direction.
function discrete_player.default.on_walk_to(self, new_pos, direction)
	return not minetest.registered_nodes[minetest.get_node(new_pos).name].walkable
end

-- Called after the player finishes the walk and lands on a stable spot.
function discrete_player.default.on_walk_finish(self)

end

-- Called right after the player joins the game and gets attached to the discrete_player entity.
function discrete_player.default.after_attach(self)

end

-- Called each step when the player isn't walking
function discrete_player.default.custom_on_step(self)

end

--[[*****************************************************
    * These functions are NOT designed to be overriden. *
    *****************************************************]]--

-- Finishes walking immediatelly.
function discrete_player.default.force_end_move(self)
	if self.walking_direction ~= nil then
		self.end_walk_functions[self.walking_direction](self)
	end
end

-- Tries walking to a direction. Returns true when successful.
function discrete_player.default.try_walk_to(self, direction)
	local pos = self.object:get_pos()
	if self:on_walk_to(vector.add(pos, self.directions[direction]), direction) then
		self.start_walking_functions[direction](self, pos)
		return true
	end
	return false
end

-- Attaches a player to the discrete_player entity
function discrete_player.default.attach_player(self, player, name)
	self.player = name
	player:set_attach(self.object, "",
		{x = 0, y = -5, z = 0}, {x = 0, y = 0, z = 0})
	player_api.player_attached[name] = true
	discrete_player.bank[name] = self
	discrete_player.current = self
	self:after_attach()
end

-- This is called each step. It handles keyboard input.
function discrete_player.default.on_step(self, dtime)
	local pos = self.object:get_pos()
	if self.player == nil then
		self.object:remove()
		return
	end
	if self.walking_direction ~= nil then
		self.walk_stop_functions[self.walking_direction](self, pos)
		return
	end
	self.custom_on_step()
	if not self.active then
		return
	end
	
	local player_objref = minetest.get_player_by_name(self.player)
	if player_objref then
		local ctrl = player_objref:get_player_control()
		if ctrl.jump then
			self:try_walk_to("yp")
			return
		end
		if ctrl.sneak then
			self:try_walk_to("yn")
			return
		end
		
		if ctrl.left or ctrl.right or ctrl.up or ctrl.down then
			local yaw = player_objref:get_look_horizontal() / math.pi * 2 + 4
			if ctrl.left then
				yaw = yaw + 1
			elseif ctrl.down then
				yaw = yaw + 2
			elseif ctrl.right then
				yaw = yaw + 3
			end
		
			yaw = math.floor(yaw + 0.5) % 4
			self.object:set_yaw(yaw * math.pi/2)
			
			if yaw == 0 then
				self:try_walk_to("zp")
			elseif yaw == 1 then
				self:try_walk_to("xn")
			elseif yaw == 2 then
				self:try_walk_to("zn")
			elseif yaw == 3 then
				self:try_walk_to("xp")
			end
			return
		end
	end	
end

-- Create a discrete player according to the properties in table_override.
-- If anything isn't set in table_override, one of the methods above is used instead.
function discrete_player.make_player(table_override)
	minetest.register_on_joinplayer(function(player)
		if creative and creative.is_enabled_for(player:get_player_name()) then
			return
		end
		
		local pos = player:get_pos()
		pos.x = math.floor(pos.x + 0.5)	
		pos.y = math.floor(pos.y + 0.5)	
		pos.z = math.floor(pos.z + 0.5)
		local name = player:get_player_name()
		local dp = minetest.add_entity(pos, "discrete_player:discrete_player")
		local dpe = dp:get_luaentity()
		dpe:attach_player(player, name)
	end)

	minetest.register_on_leaveplayer(function(player, timed_out)
		local parent = player:get_attach();
		if parent then
			local dpe = parent:get_luaentity()
			dpe:force_end_move()
			dpe.object:remove()
		end
		discrete_player.bank[player:get_player_name()] = nil
	end)

	minetest.register_on_shutdown(function()
		for _, e in pairs(discrete_player.bank) do
			if (e.force_end_move ~= nil) then
				local player_objref = minetest.get_player_by_name(e.player)
				e:force_end_move()
				local pos = e.object:get_pos()
				e.object:remove()
				player_objref:set_pos(pos)
			end
		end
	end)
	
	for k,v in pairs(table_override) do
		discrete_player.default[k] = v
	end

	minetest.register_entity(":discrete_player:discrete_player", discrete_player.default)
end

-- Uncomment to show the example
--[[
local modpath = minetest.get_modpath("discrete_player")
dofile(modpath .. "/example.lua")
--]]

