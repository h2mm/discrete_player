discrete_player.make_player({
	-- Properties of the entity
	initial_properties = {
		physical = false,
		collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
		visual = "cube",
		textures = {"default_glass.png","default_glass.png","default_glass.png","default_glass.png","default_glass.png","default_glass.png"},
	},
	
	-- Allow walking through all nodes which are normally not walkable.
	on_walk_to = function(self, new_pos, direction)
		return not minetest.registered_nodes[minetest.get_node(new_pos).name].walkable
	end,
	
	-- Print the new player's position
	on_walk_finish = function(self)
		local pos = self.object:get_pos()
		minetest.chat_send_all("went on pos X="..pos.x.." Y="..pos.y.." Z="..pos.z)
	end,
	
	-- Display player's name
	after_attach = function(self)
		minetest.chat_send_all("player "..self.player.." started")
	end,
})

